# SpringbootLean

#### 介绍
基于 Spring Boot + Spring + Mybatis 搭建了一个简单的项目，主要实现了系统管理相关的功能。可基于此模板进行业务功能开发。
搭建项目的相关博客：[<font face="黑体" >**光头才能强**</font>](https://www.cnblogs.com/dz-boss/) Spring Boot 入门系列博客
#### 技术选项
1.后台
* 基于 Spring Boot + Spring + Mybatis 基本框架搭建项目
* 基于 Shiro 进行登录验证和权限管理
* 基于 PageHelper 进行分页管理
* 基于 logback 进行日志管理
* 基于Poi、jxl 和 esayExcel 三种方式导出 excel
* 集成DataWay配置接口

2.前端
* 基于 FreeMarker 模板引擎编写页面
* 基于 ZTree 和 TreeTable 制作树形图
* 基于 WebSocket 实时显示系统日志

3.数据库
* 基于 MySQL 存储数据
* 基于 Redis 进行缓存管理（Mybatis二级缓存），并通过 Sentinel 配置 Redis 主从模式

#### 页面效果
* 主页面，显示系统实时日志
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/main.png)
* 用户列表
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/userlist.png)
* 更新用户
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/userupdate.png)
* 角色列表
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/rolelist.png)
* 更新角色
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/roleupdate.png)
* 菜单列表
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/menulist.png)
* 更新菜单
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/menuupdate.png)
* 在线用户
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/force.png)
* DataWay接口配置
![image text](https://gitee.com/bald_dz/SpringbootLean/raw/master/src/main/resources/image/dataWay.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
