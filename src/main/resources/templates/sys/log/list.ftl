<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>日志列表</title>
</head>
<body>
    <div class="box-body">
        <table id="log_tab" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th><input type="checkbox" title="全选" /></th>
                    <th>序号</th>
                    <th>用户id</th>
                    <th>用户名</th>
                    <th>操作</th>
                    <th>用时</th>
                    <th>方法</th>
                    <th>参数</th>
                    <th>IP地址</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
            </thead>
        </table>
    </div>
    <script  type="text/javascript">
        var log_tab;
        $(function () {
            var url="/sys/log/listPage";
            log_tab = $('#log_tab').DataTable({
                "fnDrawCallback": function () {
                },
                "dom": '<"top"i>rt<"bottom"flp><"clear">',
                "processing": true,
                "searching": false,
                "serverSide": true,   //启用服务器端分页
                "order": [[ 8, "asc" ]],//默认排序字段
                "bInfo": true,
                "bAutoWidth": false,
                "scrollX": true,
                "scrollCollapse": false,
                "language":{"url":"/plugins/datatables/language.json"},
                "ajax":{"url":url,"type":"post"},
                "columns":[
                    {"data":"id"},
                    {"data":"id"},
                    {"data":"userId"},
                    {"data":"username"},
                    {"data":"operation"},
                    {"data":"time"},
                    {"data":"method"},
                    {"data":"params"},
                    {"data":"ip"},
                    {"data":"createTime"},
                    {"data":"id"}
                ],
                "columnDefs" : [
                    {
                        targets: 0,
                        data: null,
                        orderable:false,
                        render: function (data) {
                            return '<input type="checkbox" class="userCheckbox" value="'+data+'"/>';
                        }
                    },
                    {
                        targets: 1,
                        data: null,
                        orderable:false,
                        render: function (data) {
                            No=No+1;
                            return No;
                        }
                    },
                    {
                        "targets" : -1,
                        "data" : null,
                         orderable:false,
                        "render" : function(data) {
                            var data = "'"+data+"'";
                            var btn2 = '<a class="btn btn-warning btn-sm"  onclick="log_list_delete('+data+')"><i class="fa fa-remove"></i>删除</a> &nbsp;';
                            return btn2;
                        }
                    }
                ]
            }).on('preXhr.dt', function ( e, settings, data ) {
                No=0;
            }).on('xhr.dt', function(e, settings, json, xhr) {
            });
        });
        //删除
        function log_list_delete(id) {
            var options = {
                url:  '/sys/log/delete?id='+id,
                type: 'get',
                dataType: 'text',
                success: function (data) {
                    log_tab.draw(false);
                    alertMsg("<p>删除成功</p>","success");
                }
            };
            $.ajax(options);
        }
    </script>
</body>
</html>