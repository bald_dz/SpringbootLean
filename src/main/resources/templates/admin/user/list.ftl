<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户列表</title>
</head>
<body>
<div class="box-body">
    <div class="clearfix">
        <form class="form-horizontal">
            <input id="user_list_repeatApply" name="user_list_repeatApply" type="reset" style="display:none;"/>
            <div class="form-group clearfix">
                <label class="col-md-1  control-label">用户名</label>
                <div class="col-md-2">
                    <input type="text" class="input-sm form-control" id="user_list_user_name" name="user_list_user_name"
                           placeholder="请输入登录名称...">
                </div>
                <label class="col-md-1  control-label">电话</label>
                <div class="col-md-2">
                    <input type="text" class="input-sm form-control" id="user_list_true_phone"
                           name="user_list_true_phone" placeholder="请输入电话号码...">
                </div>
                <label class="col-md-1  control-label">省</label>
                <div class="col-md-2">
                    <input type="text" class="input-sm form-control" id="user_list_province"
                           name="user_list_province" placeholder="请输入省...">
                </div>
                <button type="button" onclick="user_list_query();" class="btn btn-sm btn-primary"><i
                        class="fa fa-search"></i>搜索
                </button>
                <a class="btn btn-sm btn-success" target="modal" modal="lg" href="/admin/user/add"><i
                        class="fa fa-plus"></i>增加</a>
                <button type="button" onclick="user_list_delete('delete');" class="btn btn-sm btn-danger"><i
                        class="fa fa-remove"></i>删除
                </button>
                <button type="button" onclick="user_list_reset();" class="btn btn-sm btn-default">重置</button>
            </div>
        </form>
    </div>
    <table id="user_tab" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th><input type="checkbox" title="全选"/></th>
            <th>序号</th>
            <th>用户名</th>
            <th>电话</th>
            <th>邮箱</th>
            <th>性别</th>
            <th>省</th>
            <th>市</th>
            <th>县（区）</th>
            <th>状态</th>
            <th>更新时间</th>
            <th>操作</th>
        </tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    var user_tab;
    var user_list_param;
    $(function () {
        var url = "/admin/user/page";
        user_list_setParm();
        user_tab = $('#user_tab').DataTable({
            "fnDrawCallback": function () {
            },
            "dom": '<"top"i>rt<"bottom"flp><"clear">',
            //"ordering":false,//是否排序
            "processing": true,
            "searching": false,
            "serverSide": true,   //启用服务器端分页
            "order": [[10, "asc"]],//默认排序字段
            "bInfo": true,
            "bStateSave": true,
            "bAutoWidth": false,
            "scrollX": true,
            "scrollCollapse": false,
            /*fixedColumns:   {
                leftColumns: 0,
                rightColumns: 1
            },*/
            "language": {"url": "/plugins/datatables/language.json"},
            "ajax": {"url": url, "data": user_list_param, "type": "post"},
            "columns": [
                {"data": "id"},
                {"data": null},
                {"data": "username"},
                {"data": "mobile"},
                {"data": "email"},
                {"data": "sex"},
                {"data": "province"},
                {"data": "city"},
                {"data": "district"},
                {"data": "status"},
                {"data": "updateTime"},
                {"data": "id"}
            ],
            "columnDefs": [
                {
                    targets: 0,
                    data: null,
                    orderable: false,
                    render: function (data) {
                        return '<input type="checkbox" class="userCheckbox" value="' + data + '"/>';
                    }
                },
                {
                    targets: 1,
                    data: null,
                    orderable: false,
                    render: function (data) {
                        No = No + 1;
                        return No;
                    }
                },
                {
                    targets: 5,
                    data: null,
                    orderable: false,
                    render: function (data) {
                        if (data === 1) {
                            return '男';
                        } else {
                            return '女';
                        }
                    }
                },
                {
                    targets: 9,
                    data: null,
                    orderable: false,
                    render: function (data) {
                        if (data === 1) {
                            return '正常';
                        } else {
                            return '删除';
                        }
                    }
                },
                {
                    "targets": -1,
                    "data": null,
                    orderable: false,
                    "render": function (data) {
                        var data1 = data;
                        var data = "'" + data + "'";
                        var btn1 = '<a class="btn btn-xs btn-warning"  target="modal" modal="lg" href="/admin/user/update?id=' + data1 + '"><i class="fa fa-edit"></i>修改</a> &nbsp;';
                        var btn2 = '<a class="btn btn-xs btn-danger"     onclick="user_list_delete(' + data + ')"><i class="fa fa-remove"></i>删除</a> &nbsp;';
                        return btn1 + btn2;
                    }
                }
            ]
        }).on('preXhr.dt', function (e, settings, data) {
            No = 0;
        }).on('xhr.dt', function (e, settings, json, xhr) {
        });
    });

    //搜索框内容重置
    function user_list_reset() {
        $("input[name='user_list_repeatApply']").click();
    }

    //增加
    function user_list_add() {

    }

    //删除
    function user_list_delete(param) {
        var href = "/";
        var title = "<p>警告！ 所选取的数据将会被删除！</p>";
        var cb;
        if (param == "delete") {
            var checkNum = $('input:checkbox[class="userCheckbox"]:checked').length;
            var checkVal = [];
            if (checkNum == 0) {
                alertMsg("<p>请选择数据</p>", "warning");
                return;
            }
            $.each($('input:checkbox[class="userCheckbox"]:checked'), function () {
                checkVal.push($(this).val());
            });
            cb = "user_list_delete_data('" + checkVal + "');";
        } else {
            cb = "user_list_delete_one_data('" + param + "');";
        }
        //$("#smModal").attr("callback", cb).find(".modal-body").html(title).end().modal("show");
        $("#smModal").attr("action", href).attr("callback", cb).find(".modal-body").html(title).end().modal("show");
        //alert(cb);
        //$("#smModal").modal("show");
    }

    function user_list_delete_data(checkVal) {

        var options = {
            url: '/admin/user/delete?checkVal=' + checkVal,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data > 0) {
                    user_tab.draw(false);
                    alertMsg("<p>成功删除" + data + "条记录</p>", "success");
                } else {
                    alertMsg("<p>删除失败</p>", "danger");
                }
            }
        };
        $.ajax(options);
    }

    function user_list_delete_one_data(id) {
        var options = {
            url: '/admin/user/deleteOne?id=' + id,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                user_tab.draw(false);
                alertMsg("<p>删除成功</p>", "success");
            }
        };
        $.ajax(options);
    }

    //搜索
    function user_list_query() {
        user_list_setParm();
        user_tab.settings()[0].ajax.data = user_list_param;
        user_tab.ajax.reload();
    }

    //动态拼接参数
    function user_list_setParm() {
        var user_list_user_name = $("#user_list_user_name").val();
        var user_list_true_phone = $("#user_list_true_phone").val();
        var user_list_province = $("#user_list_province").val();
        user_list_param = {
            "user_list_user_name": user_list_user_name,
            "user_list_true_phone": user_list_true_phone,
            "user_list_province": user_list_province,
        };
    }
</script>
</body>
</html>