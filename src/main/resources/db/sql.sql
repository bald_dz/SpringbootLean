-- we don't know how to generate schema myshop (class Schema) :(
create table if not exists area
(
	id int auto_increment
		primary key,
	area_id varchar(50) null,
	area varchar(60) null,
	father varchar(6) null
);

create index father
	on area (father)
;

create table if not exists city
(
	id int auto_increment
		primary key,
	city_id varchar(6) null,
	city varchar(50) null,
	father varchar(6) null
);

create index cityID
	on city (city_id)
;

create table if not exists province
(
	id int auto_increment
		primary key,
	province_id varchar(6) null,
	province varchar(40) null
)
;

create table if not exists t_log
(
	id int auto_increment
		primary key,
	user_id int null comment '用户id',
	username varchar(50) null comment '用户名',
	operation varchar(50) null comment '操作',
	time int null comment '时间',
	method varchar(200) null comment '方法',
	params varchar(5000) null comment '参数',
	ip varchar(64) null comment 'ip',
	create_time datetime null comment '日志生成时间'
)
;

create table if not exists t_menu
(
	id int auto_increment
		primary key,
	name varchar(20) null comment '菜单名称',
	type int null comment '资源类型,1:目录，2：菜单，3：按钮',
	url varchar(200) null comment '点击后前往的地址',
	parent_id int null comment '父编号',
	parent_ids varchar(100) null comment '父编号列表',
	permission varchar(100) null comment '权限字符串',
	menu_icon varchar(255) null comment '图标',
	remarks varchar(255) null comment '摘要',
	create_by varchar(32) null,
	create_date datetime null,
	update_by varchar(32) null,
	update_date datetime null,
	del_flag int null
)
;

create table if not exists t_report
(
	id int auto_increment
		primary key,
	report_id varchar(40) null comment '报表id',
	col1 int null,
	col2 int null,
	col3 int null,
	col4 int null,
	col5 int null,
	col6 int null,
	col7 int null,
	col8 int null,
	col9 int null,
	col10 int null,
	col11 varchar(20) null,
	col12 varchar(20) null,
	col13 varchar(20) null,
	col14 varchar(20) null,
	col15 varchar(20) null,
	col16 varchar(20) null,
	col17 varchar(20) null,
	col18 varchar(20) null,
	col19 varchar(20) null,
	col20 varchar(20) null,
	col21 datetime null,
	col22 datetime null,
	col23 datetime null,
	col24 datetime null,
	col25 datetime null,
	col26 datetime null,
	col27 datetime null,
	col28 datetime null,
	col29 datetime null,
	col30 datetime null
)
;

create table if not exists t_role
(
	id int auto_increment
		primary key,
	role_name varchar(10) not null comment '角色名称',
	role_remark varchar(100) null comment '角色描述',
	user_id_create int not null comment '创建角色用户id',
	create_time datetime not null comment '创建时间',
	update_time datetime not null comment '修改时间'
)
;

create table if not exists t_role_menu
(
	id int auto_increment
		primary key,
	role_id int null comment '角色id',
	menu_id int null comment '菜单id'
)
;

create table if not exists t_user
(
	id int auto_increment
		primary key,
	username varchar(20) not null comment '用户名',
	password varchar(40) not null comment '登陆密码',
	email varchar(30) not null comment '邮箱',
	mobile varchar(15) not null comment '手机号',
	status int not null comment '状态 0：正常，1：删除',
	create_time datetime not null comment '创建时间',
	update_time datetime not null comment '修改时间',
	sex int not null comment '性别',
	province varchar(30) not null comment '位置，省',
	city varchar(30) not null comment '位置，市',
	district varchar(30) not null comment '位置，县'
)
;

create table if not exists t_user_role
(
	id int auto_increment
		primary key,
	user_id int null comment '用户id',
	role_id int null comment '角色id'
)
;

