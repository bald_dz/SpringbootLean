package com.learn.hello.system.common.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ExcelListener
 * @Deccription 通过esayExcel的方式导出excel
 * @Author DZ
 * @Date 2020/1/20 22:28
 **/
@Slf4j
public class ExcelListener extends AnalysisEventListener<T> {
    //可以通过实例获取该值
    private final List<T> rows = new ArrayList<>();

    @Override
    public void invoke(T object, AnalysisContext analysisContext) {
        //数据存储到list，供批量处理，或后续自己业务逻辑处理。
        rows.add(object);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    }

    public List<T> getRows() {
        return rows;
    }
}
