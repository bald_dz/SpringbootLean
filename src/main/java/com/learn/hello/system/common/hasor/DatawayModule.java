package com.learn.hello.system.common.hasor;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @ClassName DatawayModule
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/5/13 21:48
 **/
@DimModule
@Component
public class DatawayModule implements SpringModule {
    @Autowired
    private DataSource dataSource = null;
    @Override
    public void loadModule(ApiBinder apiBinder) throws Throwable {
        apiBinder.installModule(new JdbcModule(Level.Full, this.dataSource));
    }
}
