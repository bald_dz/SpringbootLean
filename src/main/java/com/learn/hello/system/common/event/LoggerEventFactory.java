package com.learn.hello.system.common.event;

import com.lmax.disruptor.EventFactory;

/**
 * Content :进程日志事件工厂类
 */
public class LoggerEventFactory implements EventFactory<LoggerEvent> {
    @Override
    public LoggerEvent newInstance() {
        return new LoggerEvent();
    }
}
