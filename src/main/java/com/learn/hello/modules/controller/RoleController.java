package com.learn.hello.modules.controller;

import com.learn.hello.modules.entity.Role;
import com.learn.hello.modules.entity.ZTree;
import com.learn.hello.modules.service.MenuService;
import com.learn.hello.modules.service.RoleMenuService;
import com.learn.hello.modules.service.RoleService;
import com.learn.hello.system.common.annotation.Log;
import com.learn.hello.system.utils.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @ClassName RoleController
 * @Deccription 角色管理
 * @Author DZ
 * @Date 2020/1/4 11:20
 **/
@Controller
@RequestMapping(value = "admin/role")
public class RoleController {
    private static final String[] COLS_ORDER = {"", "", "role_name", "role_remark", "user_id_create", "create_time", "update_time", ""};

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleMenuService roleMenuService;

    @Autowired
    private MenuService menuService;

    @Log("返回角色页面")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public void list() {
    }

    @Log("查看角色")
    @ResponseBody
    @RequestMapping(value = "page", method = RequestMethod.POST)
    public PageUtil<Role> page(@RequestParam(value = "start", defaultValue = "1") int start,
                               @RequestParam(value = "length", defaultValue = "10") int pageSize,
                               @RequestParam(value = "role_list_role_name", required = false) String roleName,
                               @RequestParam(value = "role_list_role_remark", required = false) String roleRemark,
                               @RequestParam(value = "role_list_role_user_create", required = false) String usernameCreate,
                               @RequestParam(value = "order[0][column]", required = false) Integer orderIndex,
                               @RequestParam(value = "order[0][dir]", required = false) String orderDir) {
        return roleService.page((start / pageSize) + 1, pageSize, roleName, roleRemark, usernameCreate, COLS_ORDER[orderIndex], orderDir);
    }
    @Log("更新角色")
    @RequestMapping(value = "update")
    public void update(int id, ModelMap modelMap) {
        Role role = roleService.selectById(id);
        modelMap.put("role", role);
    }

    @ResponseBody
    @RequestMapping(value = "/roleUpdateTree", method = RequestMethod.POST)
    public List<ZTree> roleUpdateTree(String roleId) {
        return roleService.getAllMenuByRoleId(roleId);
    }

    @Log("保存角色")
    @ResponseBody
    @RequestMapping(value = "/roleUpdateSave", method = RequestMethod.POST)
    public String roleUpdateSave(Role role, String menuId, String createTimeName) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        role.setUpdateTime(new Date());
        role.setCreateTime(sdf.parse(createTimeName));
        roleService.roleUpdateSave(role);
        //修改菜单权限
        roleMenuService.roleUpdateSave(role.getId(), menuId);
        return "0";
    }

    @Log("删除角色")
    @ResponseBody
    @RequestMapping(value = "/delete")
    public int delete(String ids) {
        return roleService.deleteRole(ids);
    }

    @Log("删除一个角色")
    @ResponseBody
    @RequestMapping(value = "/deleteOne")
    public int deleteOne(String id) {
        return roleService.deleteRole(id);
    }

    @Log("返回增加角色列表")
    @RequestMapping("/add")
    public void add() {
    }

    @ResponseBody
    @RequestMapping(value = "/roleTree", method = RequestMethod.POST)
    public List<ZTree> roleTree() {
        return menuService.getAllMenus();
    }

    @Log("增加角色")
    @ResponseBody
    @RequestMapping(value = "/saveRole", method = RequestMethod.POST)
    public String saveRole(Role role, String menuId) {
        roleService.saveRole(role, menuId);
        return "0";
    }
}
