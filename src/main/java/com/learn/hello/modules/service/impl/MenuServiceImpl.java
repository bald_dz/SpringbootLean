package com.learn.hello.modules.service.impl;

import com.learn.hello.modules.entity.Menu;
import com.learn.hello.modules.entity.ZTree;
import com.learn.hello.modules.mapper.MenuMapper;
import com.learn.hello.modules.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName MenuServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/5 11:08
 **/
@Service
@Slf4j
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> getMenuByParentId(int parentId) {
        ModelMap modelMap = new ModelMap();
        modelMap.put("parentId", parentId);
        return menuMapper.selectMenusByParentId(modelMap);
    }

    @Override
    public List<Menu> getSecondMenuByParentId(List<Menu> listParent) {
        StringBuilder sb = new StringBuilder();
        List list = new ArrayList();
        for (Menu m : listParent) {
            sb.append(m.getId()).append(",");
        }
        return menuMapper.selectMenusByParentIds(sb.toString().substring(0, sb.length() - 1));
    }

    @Override
    public int getchildNodNumByParentId(Integer parentId) {
        Example example = new Example(Menu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("parentId", parentId);
        return menuMapper.selectCountByExample(example);
    }

    @Override
    public void deleteById(int id) {
        Menu menu = new Menu();
        menu.setId(id);
        menu.setDelFlag(1);
        menuMapper.deleteById(menu);
    }

    @Override
    public Menu getMenuById(int id) {
        return menuMapper.selectMenuById(id);
    }

    @Override
    public void save(Menu menu) {
        menu.setCreateBy("2");
        menu.setCreateDate(new Date());
        menu.setUpdateDate(new Date());
        menu.setUpdateBy("2");
        menu.setDelFlag(2);
        menuMapper.insertSelective(menu);
        log.info("{}", menu);
    }

    @Override
    public void saveUpdate(Menu menu) {
        menuMapper.updateByPrimaryKeySelective(menu);
    }

    @Override
    public List<ZTree> getAllMenus() {
        List<Menu> menus = menuMapper.selectAllMenus();
        List<ZTree> zTrees = new ArrayList<>();
        for (Menu menu : menus) {
            ZTree zt = new ZTree();
            zt.setId(menu.getId());
            zt.setpId(menu.getParentId());
            zt.setName(menu.getName());
            if (menu.getParentId().toString().length() == 1) {
                zt.setOpen(true);
            } else {
                zt.setOpen(false);
            }
            zt.setChecked(false);
            zTrees.add(zt);
        }
        return zTrees;
    }
}
