package com.learn.hello.modules.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.hello.modules.entity.TLog;
import com.learn.hello.modules.entity.User;
import com.learn.hello.modules.entity.UserOnline;
import com.learn.hello.modules.service.OnlineService;
import com.learn.hello.system.utils.PageUtil;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName OnlineServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/12 14:28
 **/
@Service
public class OnlineServiceImpl implements OnlineService {

    private final SessionDAO sessionDAO;

    @Autowired
    public OnlineServiceImpl(SessionDAO sessionDAO) {
        this.sessionDAO = sessionDAO;
    }

    // 在线用户列表
    @Override
    public PageUtil<UserOnline> queryForPage(int pageCurrent, int pageSize) {
        List<UserOnline> list = new ArrayList<>();
        PageHelper.startPage(pageCurrent, pageSize);
        Collection<Session> sessions = sessionDAO.getActiveSessions();
        for (Session session : sessions) {
            UserOnline userOnline = new UserOnline();
            if (session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY) == null) {
                continue;
            } else {
                SimplePrincipalCollection principalCollection = (SimplePrincipalCollection) session
                        .getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
                User user = (User) principalCollection.getPrimaryPrincipal();
                userOnline.setUsername(user.getUsername());
            }
            userOnline.setId((String) session.getId());
            userOnline.setHost(session.getHost());
            userOnline.setStartTimestamp(session.getStartTimestamp());
            userOnline.setLastAccessTime(session.getLastAccessTime());
            userOnline.setTimeout(session.getTimeout());
            list.add(userOnline);
        }
        PageInfo<UserOnline> pageInfo = new PageInfo<UserOnline>(list, pageSize);
        PageUtil<UserOnline> resultData = new PageUtil<UserOnline>(pageCurrent, pageSize, (int) pageInfo.getTotal(), (int) pageInfo.getTotal(), pageInfo.getList());
        return resultData;
    }

    //强制下线
    @Override
    public void forceLogout(String sessionId) {
        Session session = sessionDAO.readSession(sessionId);
        session.setTimeout(0);
        //强制下线后，也可以做一些跳转工作
        // todo
    }
}
