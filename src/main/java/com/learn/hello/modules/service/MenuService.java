package com.learn.hello.modules.service;

import com.learn.hello.modules.entity.Menu;
import com.learn.hello.modules.entity.ZTree;

import java.util.List;

public interface MenuService {
    List<Menu> getMenuByParentId(int parentId);

    List<Menu> getSecondMenuByParentId(List<Menu> listParent);

    int getchildNodNumByParentId(Integer parentId);

    void deleteById(int id);

    Menu getMenuById(int id);

    void save(Menu menu);

    void saveUpdate(Menu menu);

    List<ZTree> getAllMenus();
}
