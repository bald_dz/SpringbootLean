package com.learn.hello.modules.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.hello.modules.entity.Menu;
import com.learn.hello.modules.entity.Role;
import com.learn.hello.modules.entity.RoleMenu;
import com.learn.hello.modules.entity.ZTree;
import com.learn.hello.modules.mapper.MenuMapper;
import com.learn.hello.modules.mapper.RoleMapper;
import com.learn.hello.modules.mapper.RoleMenuMapper;
import com.learn.hello.modules.service.RoleService;
import com.learn.hello.system.utils.PageUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName RoleServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/4 11:22
 **/
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public PageUtil<Role> page(int pageCurrent, int pageSize, String roleName, String roleRemark, String usernameCreate, String cols, String orderDir) {
        ModelMap modelMap = new ModelMap();
        if (StringUtils.isNotBlank(roleName)) {
            modelMap.put("roleName", "%" + roleName.trim() + "%");
        }
        if (StringUtils.isNotBlank(roleRemark)) {
            modelMap.put("roleRemark", "%" + roleRemark.trim() + "%");
        }
        if (StringUtils.isNotBlank(usernameCreate)) {
            modelMap.put("usernameCreate", "%" + usernameCreate.trim() + "%");
        }
        if (StringUtils.isNotBlank(cols)) {
            modelMap.put("cols", cols + " " + orderDir);
        }

        PageHelper.startPage(pageCurrent, pageSize);
        List<Role> roles = roleMapper.selectByCondition(modelMap);
        PageInfo<Role> pageInfo = new PageInfo<Role>(roles, pageSize);
        return new PageUtil<Role>(pageCurrent, pageSize, (int) pageInfo.getTotal(), (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Role selectById(int id) {
        return roleMapper.selectById(id);
    }

    @Override
    public List<ZTree> getAllMenuByRoleId(String roleId) {
        // 1.查询所有的菜单
        List<Menu> menus = menuMapper.selectAllMenus();
        // 2.查询改角色下所拥有的菜单id
        List<Integer> menusByRoleId = getMenusByRoleId(roleId);
        // 3.初始化ztree
        List<ZTree> zTrees = new ArrayList<>();
        for (Menu menu : menus) {
            ZTree zt = new ZTree();
            zt.setId(menu.getId());
            zt.setpId(menu.getParentId());
            zt.setName(menu.getName());
            if (menu.getParentId().toString().length() == 1) {
                zt.setOpen(true);
            } else {
                zt.setOpen(false);
            }
            if (menusByRoleId.contains(menu.getId())) {
                zt.setChecked(true);
            } else {
                zt.setChecked(false);
            }
            zTrees.add(zt);
        }
        return zTrees;
    }

    @Override
    public void roleUpdateSave(Role role) {
        roleMapper.updateByPrimaryKeySelective(role);
    }

    @Override
    public int deleteRole(String ids) {
        String[] idStr = ids.split(",");
        ArrayList<Integer> list = new ArrayList();
        for (String str : idStr) {
            list.add(Integer.parseInt(str));
        }
        //删除角色
        deleteRolesByIds(list);
        // 删除角色对应的菜单权限
        deleteRoleMenusByIds(list);
        return idStr.length;
    }

    @Override
    public void saveRole(Role role, String menuIds) {
        //插入角色表
        role.setUserIdCreate(2);
        role.setCreateTime(new Date());
        role.setUpdateTime(new Date());
        roleMapper.insertSelective(role);
        // 插入角色菜单表
        if (StringUtils.isNotBlank(menuIds)) {
            String[] menuId = menuIds.split(",");
            for (String str : menuId) {
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setRoleId(role.getId());
                roleMenu.setMenuId(Integer.parseInt(str));
                roleMenuMapper.insert(roleMenu);
            }
        }
    }

    @Override
    public List<Role> selectAllRole() {
        return roleMapper.selectAllRole();
    }

    private void deleteRoleMenusByIds(ArrayList<Integer> ids) {
        Example example = new Example(RoleMenu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("roleId", ids);
        roleMenuMapper.deleteByExample(example);
    }

    private void deleteRolesByIds(ArrayList<Integer> ids) {
        Example example = new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", ids);
        roleMapper.deleteByExample(example);
    }

    private List<Integer> getMenusByRoleId(String roleId) {
        Example example = new Example(RoleMenu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("roleId", roleId);
        List<RoleMenu> roleMenus = roleMenuMapper.selectByExample(example);
        List<Integer> list = new ArrayList<>();
        for (RoleMenu roleMenu : roleMenus) {
            list.add(roleMenu.getMenuId());
        }
        return list;
    }
}
