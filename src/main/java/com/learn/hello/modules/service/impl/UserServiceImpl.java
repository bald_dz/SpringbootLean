package com.learn.hello.modules.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.hello.modules.entity.*;
import com.learn.hello.modules.mapper.*;
import com.learn.hello.modules.service.UserService;
import com.learn.hello.system.utils.PageUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * @ClassName TbUserServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2019/12/26 22:39
 **/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProvinceMapper provinceMapper;

    @Autowired
    private CityMapper cityMapper;

    @Autowired
    private AreaMapper areaMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public PageUtil<User> page(int pageCurrent, int pageSize, String username, String mobile, String province, String cols, String orderDir) {
        Example example = new Example(User.class);
        example.setOrderByClause(cols + " " + orderDir);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(username)) {
            criteria.andLike("username", "%" + username.trim() + "%");
        }
        if (StringUtils.isNotBlank(mobile)) {
            criteria.andLike("mobile", "%" + mobile.trim() + "%");
        }
        if (StringUtils.isNotBlank(province)) {
            criteria.andLike("province", "%" + province.trim() + "%");
        }
        PageHelper.startPage(pageCurrent, pageSize);
        PageInfo<User> pageInfo = new PageInfo<User>(userMapper.selectByExample(example), pageSize);
        return new PageUtil<User>(pageCurrent, pageSize, (int) pageInfo.getTotal(), (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public int delete(String[] ids) {
        List<String> idList = Arrays.asList(ids);
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", idList);
        User user = new User();
        user.setStatus(2);
        return userMapper.updateByExampleSelective(user, example);
    }

    @Override
    public void deleteOne(String id) {
        User user = new User();
        user.setStatus(2);
        user.setId(Integer.parseInt(id));
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public List<Province> getProvince() {
        return provinceMapper.selectAll();
    }

    @Override
    public List<City> getCityByParentId(String provinceId) {
        Example example = new Example(City.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("father", provinceId);
        return cityMapper.selectByExample(example);
    }

    @Override
    public List<Area> getAreaByParentId(String cityId) {
        Example example = new Example(Area.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("father", cityId);
        return areaMapper.selectByExample(example);
    }

    @Override
    public void save(User user) {
        // 保存用户
        user.setStatus(1);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        userMapper.insertSelective(user);
    }

    @Override
    public User getUserByUserid(String userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Override
    public void updateUser(User user) {
        user.setUpdateTime(new Date());
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public User selectUserByName(String username) {
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username", username);
        List<User> users = userMapper.selectByExample(example);
        if (users.isEmpty()) {
            return new User();
        } else {
            return users.get(0);
        }
    }

}
