package com.learn.hello.modules.service;

import com.learn.hello.modules.entity.Report;
import com.learn.hello.system.utils.PageUtil;

import java.util.List;

public interface ReportService {
    PageUtil<Report> queryForPage(int pageCurrent, int pageSize);

    List<Report> getAllDate();
}
