package com.learn.hello.modules.service;

import com.learn.hello.modules.entity.UserOnline;
import com.learn.hello.system.utils.PageUtil;

/**
 * @ClassName OnlineService
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/12 14:28
 **/
public interface OnlineService {
    PageUtil<UserOnline> queryForPage(int i, int pageSize);

    void forceLogout(String sessionId);
}
