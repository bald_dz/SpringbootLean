package com.learn.hello.modules.service;


import com.learn.hello.modules.entity.Area;
import com.learn.hello.modules.entity.City;
import com.learn.hello.modules.entity.Province;
import com.learn.hello.modules.entity.User;
import com.learn.hello.system.utils.PageUtil;

import java.util.List;


/**
 * @ClassName UserService
 * @Deccription TODO
 * @Author DZ
 * @Date 2019/12/26 22:39
 **/
public interface UserService {

    PageUtil<User> page(int pageCurrent, int pageSize, String userName, String mobile, String province, String col, String orderDir);

    int delete(String[] ids);

    void deleteOne(String id);

    List<Province> getProvince();

    List<City> getCityByParentId(String provinceId);

    List<Area> getAreaByParentId(String cityId);

    void save(User user);

    User getUserByUserid(String userId);

    void updateUser(User user);

    User selectUserByName(String username);
}
