package com.learn.hello.modules.mapper;

import com.learn.hello.modules.entity.TLog;
import com.learn.hello.system.mymapper.MyMapper;

public interface TLogMapper extends MyMapper<TLog> {
}