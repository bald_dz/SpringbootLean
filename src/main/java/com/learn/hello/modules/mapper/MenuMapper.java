package com.learn.hello.modules.mapper;

import com.learn.hello.modules.entity.Menu;
import com.learn.hello.system.mymapper.MyMapper;
import com.learn.hello.system.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.ui.ModelMap;

import java.util.List;
@CacheNamespace(implementation = RedisCache.class)
public interface MenuMapper extends MyMapper<Menu> {
    List<Menu> selectMenusByParentId(ModelMap modelMap);

    List<Menu> selectMenusByParentIds(String parentIds);

    void deleteById(Menu menu);

    Menu selectMenuById(int id);

    List<Menu> selectAllMenus();
}