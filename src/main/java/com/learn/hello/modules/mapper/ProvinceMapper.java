package com.learn.hello.modules.mapper;

import com.learn.hello.modules.entity.Province;
import com.learn.hello.system.mymapper.MyMapper;
import com.learn.hello.system.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;

@CacheNamespace(implementation = RedisCache.class)
public interface ProvinceMapper extends MyMapper<Province> {
}