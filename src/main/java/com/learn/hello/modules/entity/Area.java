package com.learn.hello.modules.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class Area implements Serializable {
    private static final long serialVersionUID = -4008454465976880847L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "area_id")
    private String areaId;

    private String area;

    private String father;

}