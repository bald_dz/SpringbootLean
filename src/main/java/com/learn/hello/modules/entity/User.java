package com.learn.hello.modules.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "t_user")
public class User implements Serializable {
    private static final long serialVersionUID = 7269873424966271605L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 登陆密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 状态 0：正常，1：删除
     */
    private Integer status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 位置，省
     */
    private String province;

    /**
     * 位置，市
     */
    private String city;

    /**
     * 位置，县
     */
    private String district;

}