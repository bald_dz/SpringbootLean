package com.learn.hello.modules.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "t_report")
public class Report extends BaseRowModel {
    @ExcelProperty(value = "id", index = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 报表id
     */
    @ExcelProperty(value = "报表id", index = 1)
    @Column(name = "report_id")
    private String reportId;

    @ExcelProperty(value = "col1", index = 2)
    private Integer col1;

    @ExcelProperty(value = "col2", index = 3)
    private Integer col2;

    @ExcelProperty(value = "col3", index = 4)
    private Integer col3;

    @ExcelProperty(value = "col4", index = 5)
    private Integer col4;

    @ExcelProperty(value = "col5", index = 6)
    private Integer col5;

    @ExcelProperty(value = "col6", index = 7)
    private Integer col6;

    @ExcelProperty(value = "col7", index = 8)
    private Integer col7;

    @ExcelProperty(value = "col8", index = 9)
    private Integer col8;

    @ExcelProperty(value = "col9", index = 10)
    private Integer col9;

    @ExcelProperty(value = "col10", index = 11)
    private Integer col10;

    @ExcelProperty(value = "col11", index = 12)
    private String col11;

    @ExcelProperty(value = "col12", index = 13)
    private String col12;

    @ExcelProperty(value = "col13", index = 14)
    private String col13;

    @ExcelProperty(value = "col14", index = 15)
    private String col14;

    @ExcelProperty(value = "col15", index = 16)
    private String col15;

    @ExcelProperty(value = "col16", index = 17)
    private String col16;

    @ExcelProperty(value = "col17", index = 18)
    private String col17;

    @ExcelProperty(value = "col18", index = 19)
    private String col18;

    @ExcelProperty(value = "col19", index = 20)
    private String col19;

    @ExcelProperty(value = "col20", index = 21)
    private String col20;

    @ExcelProperty(value = "col21", index = 22)
    private Date col21;

    @ExcelProperty(value = "col22", index = 23)
    private Date col22;

    @ExcelProperty(value = "col23", index = 24)
    private Date col23;

    @ExcelProperty(value = "col24", index = 25)
    private Date col24;

    @ExcelProperty(value = "col25", index = 26)
    private Date col25;

    @ExcelProperty(value = "col26", index = 27)
    private Date col26;

    @ExcelProperty(value = "col27", index = 28)
    private Date col27;

    @ExcelProperty(value = "col28", index = 29)
    private Date col28;

    @ExcelProperty(value = "col29", index = 30)
    private Date col29;

    @ExcelProperty(value = "col30", index = 14)
    private Date col30;

}