<!DOCTYPE html>
<html lang="en">
<head">
    <meta charset="UTF-8">
    <title>在线用户列表</title>
</head>
<body>
	<div class="box-body">
		<table id="user_online_tab" class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th><input type="checkbox" title="全选" /></th>
				<th>序号</th>
				<th>用户名</th>
				<th>主机</th>
				<th>登录时间</th>
				<th>最后访问时间</th>
				<th>过期时间</th>
				<th>状态</th>
				<th>操作</th>
			</tr>
			</thead>
		</table>
	</div>
    <script  type="text/javascript">
        var user_online_tab;
        $(function () {
            var url="/sys/online/onlinePage";
            user_online_tab = $('#user_online_tab').DataTable({
                "fnDrawCallback": function () {
                },
                "dom": '<"top"i>rt<"bottom"flp><"clear">',
                //"ordering":false,//是否排序
                "processing": true,
                "searching": false,
                "serverSide": true,   //启用服务器端分页
                "order": [[ 4, "asc" ]],//默认排序字段
                "bInfo": true,
                "bAutoWidth": false,
                "scrollX": true,
                "scrollCollapse": false,
                "language":{"url":"/plugins/datatables/language.json"},
                "ajax":{"url":url,"type":"post"},
                "columns":[
                    {"data":"username"},
                    {"data":"id"},
                    {"data":"username"},
                    {"data":"host"},
                    {"data":"startTimestamp"},
                    {"data":"lastAccessTime"},
                    {"data":"timeout"},
                    {"data":"status"},
                    {"data":"id"}
                ],
                "columnDefs" : [
                    {
                        targets: 0,
                        data: null,
                        orderable:false,
                        render: function (data) {
                            return '<input type="checkbox" class="userCheckbox" value="'+data+'"/>';
                        }
                    },
                    {
                        targets: 7,
                        data: null,
                        orderable:false,
                        render: function (data) {
                            if (data == 'on_line') {
                                return '<span class="label label-success">在线</span>';
                            } else if (data == 'off_line') {
                                return '<span class="label label-primary">离线</span>';
                            }
                        }
                    },
                    {
                        "targets" : -1,
                        "data" : null,
                        orderable:false,
                        "render" : function(data) {
                            data = "'"+data+"'";
                            var d = '<a class="btn btn-warning btn-sm" href="#" title="删除"  mce_href="#" onclick="forceLogout(' + data + ')"><i class="fa fa-remove"></i></a> ';
                            return d;
                        }
                    }
                ]
            }).on('preXhr.dt', function ( e, settings, data ) {
                No=0;
            }).on('xhr.dt', function(e, settings, json, xhr) {
            });
        });
        //强制下线
        function forceLogout(sessionId) {
            var options = {
                url:  '/sys/online/forceLogout?id='+sessionId,
                type: 'get',
                dataType: 'text',
                success: function (data) {
                    user_online_tab.draw(false);
                    alertMsg("<p>下线成功</p>","success");
                }
            };
            $.ajax(options);
        }
    </script>
</body>
</html>