<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>角色列表</title>
</head>
<body>
<#setting date_format="yyyy-MM-dd">
<div class="box-body">
    <div class="clearfix">
        <form class="form-horizontal">
            <input id="role_list_repeatApply" name="role_list_repeatApply" type="reset" style="display:none;"/>
            <div class="form-group clearfix">
                <label class="col-md-1  control-label">角色名称</label>
                <div class="col-md-2">
                    <input type="text" class="input-sm form-control" id="role_list_role_name" name="role_list_role_name"
                           placeholder="请输入角色名称...">
                </div>
                <label class="col-md-1  control-label">角色称呼</label>
                <div class="col-md-2">
                    <input type="text" class="input-sm form-control" id="role_list_role_remark"
                           name="role_list_role_remark" placeholder="请输入角色称呼...">
                </div>
                <label class="col-md-1  control-label">创建用户</label>
                <div class="col-md-2">
                    <input type="text" class="input-sm form-control" id="role_list_role_user_create"
                           name="role_list_role_user_create" placeholder="请输入创建用户...">
                </div>
                <button type="button" onclick="role_list_query();" class="btn btn-sm btn-primary"><i
                        class="fa fa-search"></i>搜索
                </button>
                <a class="btn btn-sm btn-success" target="modal" modal="lg" href="/admin/role/add"><i
                        class="fa fa-plus"></i>增加</a>
                <button type="button" onclick="role_list_delete('delete');" class="btn btn-sm btn-danger"><i
                        class="fa fa-remove"></i>删除
                </button>
                <button type="button" onclick="role_list_reset();" class="btn btn-sm btn-default">重置</button>
            </div>
        </form>
    </div>
    <table id="role_tab" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th><input type="checkbox" title="全选"/></th>
            <th>序号</th>
            <th>角色名称</th>
            <th>角色称呼</th>
            <th>创建用户</th>
            <th>创建时间</th>
            <th>更新时间</th>
            <th>操作</th>
        </tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    var role_tab;
    var role_list_param;
    $(function () {
        var url = "/admin/role/page";
        role_list_setParm();
        role_tab = $('#role_tab').DataTable({
            "fnDrawCallback": function () {
            },
            "dom": '<"top"i>rt<"bottom"flp><"clear">',
            //"ordering":false,//是否排序
            "processing": true,
            "searching": false,
            "serverSide": true,   //启用服务器端分页
            "order": [[6, "asc"]],//默认排序字段
            "bInfo": true,
            "bStateSave": true,
            "bAutoWidth": false,
            "scrollX": true,
            "scrollCollapse": false,
            /*fixedColumns:   {
                leftColumns: 0,
                rightColumns: 1
            },*/
            "language": {"url": "/plugins/datatables/language.json"},
            "ajax": {"url": url, "data": role_list_param, "type": "post"},
            "columns": [
                {"data": "id"},
                {"data": null},
                {"data": "roleName"},
                {"data": "roleRemark"},
                {"data": "usernameCreate"},
                {"data": "createTime"},
                {"data": "updateTime"},
                {"data": "id"}
            ],
            "columnDefs": [
                {
                    targets: 0,
                    data: null,
                    orderable: false,
                    render: function (data) {
                        return '<input type="checkbox" class="roleCheckbox" value="' + data + '"/>';
                    }
                },
                {
                    targets: 1,
                    data: null,
                    orderable: false,
                    render: function (data) {
                        No = No + 1;
                        return No;
                    }
                },
                {
                    "targets": -1,
                    "data": null,
                    orderable: false,
                    "render": function (data) {
                        var data1 = data;
                        var data = "'" + data + "'";
                        var btn1 = '<a class="btn btn-xs btn-warning"  target="modal" modal="lg" href="/admin/role/update?id=' + data1 + '"><i class="fa fa-edit"></i>修改</a> &nbsp;';
                        var btn2 = '<a class="btn btn-xs btn-danger"     onclick="role_list_delete(' + data + ')"><i class="fa fa-remove"></i>删除</a> &nbsp;';
                        return btn1 + btn2;
                    }
                }
            ]
        }).on('preXhr.dt', function (e, settings, data) {
            No = 0;
        }).on('xhr.dt', function (e, settings, json, xhr) {
        });
    });

    //搜索框内容重置
    function role_list_reset() {
        $("input[name='role_list_repeatApply']").click();
    }

    //增加
    function user_list_add() {

    }

    //删除
    function role_list_delete(param) {
        var href = "/";
        var title = "<p>警告！ 所选取的数据将会被删除！</p>";
        var cb;
        if (param == "delete") {
            var checkNum = $('input:checkbox[class="roleCheckbox"]:checked').length;
            var checkVal = [];
            if (checkNum == 0) {
                alertMsg("<p>请选择数据</p>", "warning");
                return;
            }
            $.each($('input:checkbox[class="roleCheckbox"]:checked'), function () {
                checkVal.push($(this).val());
            });
            cb = "role_list_delete_data('" + checkVal + "');";
        } else {
            cb = "role_list_delete_one_data('" + param + "');";
        }
        $("#smModal").attr("action", href).attr("callback", cb).find(".modal-body").html(title).end().modal("show");
    }

    function role_list_delete_data(checkVal) {

        var options = {
            url: '/admin/role/delete?ids=' + checkVal,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                if (data > 0) {
                    role_tab.draw(false);
                    alertMsg("<p>成功删除" + data + "条记录</p>", "success");
                } else {
                    alertMsg("<p>删除失败</p>", "danger");
                }
            }
        };
        $.ajax(options);
    }

    function role_list_delete_one_data(id) {
        var options = {
            url: '/admin/role/deleteOne?id=' + id,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                role_tab.draw(false);
                alertMsg("<p>删除成功</p>", "success");
            }
        };
        $.ajax(options);
    }

    //搜索
    function role_list_query() {
        role_list_setParm();
        role_tab.settings()[0].ajax.data = role_list_param;
        role_tab.ajax.reload();
    }

    //动态拼接参数
    function role_list_setParm() {
        var role_list_role_name = $("#role_list_role_name").val();
        var role_list_role_remark = $("#role_list_role_remark").val();
        var role_list_role_user_create = $("#role_list_role_user_create").val();
        role_list_param = {
            "role_list_role_name": role_list_role_name,
            "role_list_role_remark": role_list_role_remark,
            "role_list_role_user_create": role_list_role_user_create,
        };
    }
</script>
</body>
</html>